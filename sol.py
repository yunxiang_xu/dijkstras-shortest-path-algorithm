class Solution:
    #heap
    nodeLeft = []
    #node position in the heap
    nodePos = []
    #key value for heap
    keyValue = []
    #nodes have been examined
    explored = set()
    #Graph
    G = {}
    def __init__(self,Gin):
        self.G = Gin
        self.keyValue = [1000000]*(len(self.G)+1)
        self.nodeLeft = [0]
        self.nodePos = [-1]*(len(self.G)+1)
    def HeapUpdate(self,v,keyV):
        self.keyValue[v] = keyV
        flag = 1
        if self.nodePos[v] == -1:
            self.nodeLeft.append(v)
            self.nodePos[v] = len(self.nodeLeft)-1
            flag = -1
        i = self.nodePos[v]
        
        while (self.keyValue[self.nodeLeft[i]] < self.keyValue[self.nodeLeft[i/2]] and i > 1):
            self.SwapInHeap(i, i/2)
            i = i/2
    def HeapExtract(self):
        minNode = self.nodeLeft[1]
        #test part
        '''if self.keyValue[minNode] != min([self.keyValue[node] for node in self.nodeLeft]):
            print 'first ', self.keyValue[minNode]
            print 'minimum ',min([self.keyValue[node] for node in self.nodeLeft])
            print [self.keyValue[j] for j in self.nodeLeft]
            print 'nodes to be analyzed', self.nodeLeft
            print self.explored
            exit()'''
        last = self.nodeLeft.pop()
        self.nodePos[minNode] = - 1
        if len(self.nodeLeft) <= 1:
            return minNode
        self.nodeLeft[1] = last
        self.nodePos[last] = 1
        i = 1
        while (True):
            try:
                #print self.nodeLeft[i]
                if (self.keyValue[self.nodeLeft[2*i]] == \
                        min ([self.keyValue[self.nodeLeft[j]] for j in [i, 2*i, 2*i+1]])):
                    self.SwapInHeap(i, 2*i)
                    i = i*2
                elif (self.keyValue[self.nodeLeft[2*i+1]] == \
                        min ([self.keyValue[self.nodeLeft[j]] for j in [i, 2*i, 2*i+1]])):
                    self.SwapInHeap(i, 2*i+1)
                    i = i *2+1
                else:
                    break
            except:
                try:
                    if (self.keyValue[self.nodeLeft[2*i]] < self.keyValue[self.nodeLeft[i]]):
                        self.SwapInHeap(i, 2*i)
                        i = i*2
                    else: break
                except:
                    break
        return minNode
    def SwapInHeap(self, i, j):
        temp = self.nodeLeft[i]
        self.nodeLeft[i] = self.nodeLeft[j]
        self.nodeLeft[j] = temp
        #position maintaining
        self.nodePos[self.nodeLeft[i]] = i
        self.nodePos[self.nodeLeft[j]] = j
    def ComputeShortestPath(self, v):
        self.explored = set()
        self.explored.add(v)
        
        self.keyValue[v] = 0
        for node in self.G[v]:
            #if v == 1: print node
            self.HeapUpdate(node,self.G[v][node])
            #test part
            '''if self.keyValue[self.nodeLeft[1]] != min([self.keyValue[node] for node in self.nodeLeft]):
                print 'first ', self.keyValue[self.nodeLeft[1]]
                print 'minimum ',min([self.keyValue[node] for node in self.nodeLeft])
                print [self.keyValue[j] for j in self.nodeLeft]
                print 'nodes to be analyzed', self.nodeLeft
                print 'node has been known',self.explored
                exit()
        for i in range(1,len(self.nodeLeft)):
            try: 
                if self.keyValue[self.nodeLeft[i]] < self.keyValue[self.nodeLeft[2*i]]: pass
                else: print 'heap wrong 2'
                if self.keyValue[self.nodeLeft[i]] < self.keyValue[self.nodeLeft[2*i+1]]: pass
                else: print 'heap wrong'
            except:  pass
        print 'heap right for now'
        print 'first ', self.keyValue[self.nodeLeft[1]]
        print 'minimum ',min([self.keyValue[node] for node in self.nodeLeft])
        print [self.keyValue[j] for j in self.nodeLeft]
        print 'nodes to be analyzed', self.nodeLeft
        print 'node has been known',self.explored
        exit()'''
        while (len(self.nodeLeft)>1):
            current = self.HeapExtract()

            #print current
            if current in self.explored:
                print 'not cool'
                continue 
            else:
                #test part
                '''if self.keyValue[self.nodeLeft[1]] != min([self.keyValue[node] for node in self.nodeLeft]):
                    print 'first ', self.keyValue[self.nodeLeft[1]]
                    print 'minimum ',min([self.keyValue[node] for node in self.nodeLeft])
                    print [self.keyValue[j] for j in self.nodeLeft]
                    print 'nodes to be analyzed', self.nodeLeft
                    print 'node has been known',self.explored
                    exit()'''
                '''for i in range(1,len(self.nodeLeft)):
                    try: 
                        if self.keyValue[self.nodeLeft[i]] < self.keyValue[self.nodeLeft[2*i]]: pass
                        else: print 'heap wrong 2'
                        if self.keyValue[self.nodeLeft[i]] < self.keyValue[self.nodeLeft[2*i+1]]: pass
                        else: print 'heap wrong'
                    except:  pass
                print 'heap right for now'
                print 'first ', self.keyValue[self.nodeLeft[1]]
                print 'minimum ',min([self.keyValue[node] for node in self.nodeLeft])
                print [self.keyValue[j] for j in self.nodeLeft]
                print 'nodes to be analyzed', self.nodeLeft
                print 'node has been known',self.explored
                exit()'''
                self.explored.add(current)
                
                for endpoint in self.G[current].keys():
                    if endpoint not in self.explored:
                        newV = self.G[current][endpoint] + self.keyValue[current]
                        if newV < self.keyValue[endpoint]:
                            #print 'updating', endpoint
                            self.HeapUpdate(endpoint, newV)
                
                

                    
            
        